/*************************************************************************
*    Sarah - Desktop Partner
*    Copyright (C) 2019-2019 Ebrahim Karimi
*
*    This program is free software: you can redistribute it and/or modify
*    it under the terms of the GNU Affero General Public License as published by
*    the Free Software Foundation, either version 3 of the License, or
*    (at your option) any later version.
*
*    This program is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU Affero General Public License for more details.
*
*    You should have received a copy of the GNU Affero General Public License
*    along with this program.  If not, see <http://www.gnu.org/licenses/>.
**************************************************************************/

#include "Client.h"


#include "../Client/ClientManager.h"


void AClient::init(SOCKET s)
{
	pipe = new Pipe(s);
}

size_t AClient::Read(char* buffer, size_t bsize)
{
	return pipe->Read(buffer, bsize);
}

bool AClient::Write(const char* buffer, size_t bsize, bool flush)
{
	return pipe->Write(buffer, bsize, flush);
}

size_t AClient::Read(std::string* ret)
{
	return pipe->Read(ret);
}

bool AClient::Write(const std::string& str, bool flush)
{
	return pipe->Write(str, flush);
}

bool AClient::Write(JSON::Object obj, bool Compressed, bool flush)
{
	return pipe->Write(obj, Compressed, flush);
}

bool AClient::hasError()
{
	return pipe->hasError();
}

SOCKET AClient::getSocket()
{
	return pipe->getSocket();
}

void AClient::Remove()
{
	delete this;
}

void AClient::disconnect()
{
	manager->Log("Closing Socket and deleting Client", LL_DEBUG);
	closesocket(pipe->getSocket());
	setIsAuthenticated(false);
	if (pipe) {
		pipe->Remove();
	}
	if (this) {
		this->Remove();
	}
}

AUTHKEY AClient::getKey()
{
	return this->authKey;
}

std::string AClient::getID()
{
	return this->id;
}

void AClient::setIsAuthenticated(bool isAuthenticated)
{

	this->isAuthenticated = isAuthenticated;
	if (isAuthenticated)
		manager->getClientManager()->addClient(getID(), this);
	else manager->getClientManager()->removeClient(getID());
}

JSON::Object AClient::serializeToJson()
{
	JSON::Object obj;
	obj.set("id", getID());
	return obj;
}
