/*************************************************************************
*    Sarah - Desktop Partner
*    Copyright (C) 2019-2019 Ebrahim Karimi
*
*    This program is free software: you can redistribute it and/or modify
*    it under the terms of the GNU Affero General Public License as published by
*    the Free Software Foundation, either version 3 of the License, or
*    (at your option) any later version.
*
*    This program is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU Affero General Public License for more details.
*
*    You should have received a copy of the GNU Affero General Public License
*    along with this program.  If not, see <http://www.gnu.org/licenses/>.
**************************************************************************/


#ifndef SARAH_ACLIENT_H
#define SARAH_ACLIENT_H

#include "../Interface/Client.h"
#include "../Pipe.h"

#include "../Interface/Manager.h"
extern IManager* manager;
#include <ctime>


struct AUTHKEY {
	std::string key;
	std::time_t time;
};
class AClient : public IClient {
public:
    void init(SOCKET s) override;

	size_t Read(char* buffer, size_t bsize) override;

    bool Write(const char *buffer, size_t bsize, bool flush) override;

    size_t Read(std::string *ret) override;

    bool Write(const std::string &str, bool flush) override;

    bool Write(JSON::Object obj,bool Compressed=true, bool flush=true) override;

	bool hasError() override;

	SOCKET getSocket()override;

	void Remove() override;
	void disconnect() override;

	AUTHKEY getKey();


	std::string getID();
protected:
    IPipe *pipe;
	bool isAuthenticated = false;
	std::string id;
	AUTHKEY authKey;

	void setIsAuthenticated(bool isAuthenticated);


	JSON::Object serializeToJson() ;
};

#endif //SARAH_ACLIENT_H