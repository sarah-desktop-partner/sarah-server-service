/*************************************************************************
*    Sarah - Desktop Partner
*    Copyright (C) 2019-2019 Ebrahim Karimi
*
*    This program is free software: you can redistribute it and/or modify
*    it under the terms of the GNU Affero General Public License as published by
*    the Free Software Foundation, either version 3 of the License, or
*    (at your option) any later version.
*
*    This program is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU Affero General Public License for more details.
*
*    You should have received a copy of the GNU Affero General Public License
*    along with this program.  If not, see <http://www.gnu.org/licenses/>.
**************************************************************************/


#ifndef SARAH_ALOGGER_H
#define SARAH_ALOGGER_H

#include "../Interface/Logger.h"

class ALogger : public ILogger
{
public:
    void setLogLevel(LOG_LEVEL logLevel){
        this->log_level=logLevel;
    }
    LOG_LEVEL getLogLevel(){return log_level;}

private:
    LOG_LEVEL log_level=LL_INFO;
};
#endif //SARAH_ALOGGER_H
