#pragma once

/*************************************************************************
*    Sarah - Desktop Partner
*    Copyright (C) 2019-2019 Ebrahim Karimi
*
*    This program is free software: you can redistribute it and/or modify
*    it under the terms of the GNU Affero General Public License as published by
*    the Free Software Foundation, either version 3 of the License, or
*    (at your option) any later version.
*
*    This program is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU Affero General Public License for more details.
*
*    You should have received a copy of the GNU Affero General Public License
*    along with this program.  If not, see <http://www.gnu.org/licenses/>.
**************************************************************************/


#ifndef SARAH_AMESSAGE_HANDLER_H
#define SARAH_AMESSAGE_HANDLER_H
#include "../Interface/Object.h"
#include "../json/jsonUrbackup.h"
template<class T>
class AMessageHandler : public ISarahObject
{
public:
	explicit AMessageHandler(T* clnt) { this->client = clnt; }

	void AuthenticationRequired() {
		JSON::Object o;
		o.set("type", "Authentication");
		o.set("error", "Forbidden to access this part");
		o.set("code", 403);
		o.set("msg", "Please Authenticate to access this page first");
		client->Write(o, true, false);
	};
	void AuthenticationFailed() {

		JSON::Object o;
		o.set("type", "Authentication");
		o.set("error", "Unauthorized");
		o.set("code", 401);
		o.set("msg", "Authentication failed");
		client->Write(o, true, false);
	};

	void AuthenticationSucceeded() {
		JSON::Object o;
		o.set("type", "Authentication");
		o.set("error", "OK");
		o.set("code", 200);
		o.set("msg", "Authentication Succeeded");
		client->Write(o, true, false);
	}
protected:
	T *client;
};
#endif // !SARAH_AMESSAGE_HANDLER_H
