/*************************************************************************
*    Sarah - Desktop Partner
*    Copyright (C) 2019-2019 Ebrahim Karimi
*
*    This program is free software: you can redistribute it and/or modify
*    it under the terms of the GNU Affero General Public License as published by
*    the Free Software Foundation, either version 3 of the License, or
*    (at your option) any later version.
*
*    This program is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU Affero General Public License for more details.
*
*    You should have received a copy of the GNU Affero General Public License
*    along with this program.  If not, see <http://www.gnu.org/licenses/>.
**************************************************************************/

#include <iostream>
#ifndef _WIN32
#include <sybdb.h>
#include <cstring>
#endif
#include "BroadcastSender.h"
#include "Manager.h"
#include "json/jsonUrbackup.h"
#include "settingsValues.h"

extern IManager *manager;

BroadcastSender::BroadcastSender(const std::string &servername, unsigned short int port) : _uport(port),
                                                                                           servername(servername),
                                                                                           has_error(false) {

    s = socket(AF_INET, SOCK_DGRAM, 0);
    int optval = 1;
    int rc = setsockopt(s, SOL_SOCKET, SO_REUSEADDR, (char *) &optval, sizeof(int));
    if (rc == SOCKET_ERROR) {
        manager->Log("Failed setting SO_REUSEADDR in CUDPThread::CUDPThread", LL_ERROR);
        return;
    }

    BOOL val=TRUE;
    rc=setsockopt(s, SOL_SOCKET, SO_BROADCAST, (char*)&val, sizeof(BOOL) );
    if(rc!=0){
        manager->Log("Couldn't set SO_BROADCAST",LL_ERROR);
        return;
    }
    memset(&send_addr, 0, sizeof(send_addr));
    send_addr.sin_family = AF_INET;
    send_addr.sin_port = htons(_uport);
	send_addr.sin_addr.s_addr = INADDR_BROADCAST;
}

void BroadcastSender::operator()() {
    manager->Log("Broadcasting Started ...",LL_INFO);
    JSON::Object broadcastMSG;
    broadcastMSG.set("id",manager->getUniqueID());
    broadcastMSG.set("name",servername);
    broadcastMSG.set("version",SARAH_VERSION);
    std::string broadcastMSGstr = broadcastMSG.stringify(true);
    do{

		if (sendto(s, &broadcastMSGstr[0], broadcastMSGstr.size(), 0,
			(struct sockaddr*) & send_addr, sizeof send_addr) == SOCKET_ERROR) {
			manager->Log("Error Sending Broadcast, waiting 10 seconds and retry", LL_WARN);
			last_broadcast_ok=false;
			manager->wait(10000);
		}
		else {
		    if (!last_broadcast_ok){
                manager->Log("Starting Broadcast again", LL_INFO);
                last_broadcast_ok = true;
            }
			manager->wait(5000);
		}
	}while (!has_error);
}

