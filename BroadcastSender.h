/*************************************************************************
*    Sarah - Desktop Partner
*    Copyright (C) 2019-2019 Ebrahim Karimi
*
*    This program is free software: you can redistribute it and/or modify
*    it under the terms of the GNU Affero General Public License as published by
*    the Free Software Foundation, either version 3 of the License, or
*    (at your option) any later version.
*
*    This program is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU Affero General Public License for more details.
*
*    You should have received a copy of the GNU Affero General Public License
*    along with this program.  If not, see <http://www.gnu.org/licenses/>.
**************************************************************************/


#ifndef SARAH_BROADCASTSENDER_H
#define SARAH_BROADCASTSENDER_H
#ifdef _WIN32
#include <WinSock2.h>
#else
#include "socket_header.h"
#endif
#include "Interface/Thread.h"
#include <string>

class BroadcastSender : public IThread
{
public:
    explicit BroadcastSender(const std::string& servername,unsigned short int port);

    void operator()()override;

private:
    SOCKET s;

    unsigned short int _uport;

    bool has_error;

    bool last_broadcast_ok=true;

    std::string servername;

    sockaddr_in send_addr;
};

#endif //SARAH_BROADCASTSENDER_H
