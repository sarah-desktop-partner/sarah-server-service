/*************************************************************************
*    Sarah - Desktop Partner
*    Copyright (C) 2019-2019 Ebrahim Karimi
*
*    This program is free software: you can redistribute it and/or modify
*    it under the terms of the GNU Affero General Public License as published by
*    the Free Software Foundation, either version 3 of the License, or
*    (at your option) any later version.
*
*    This program is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU Affero General Public License for more details.
*
*    You should have received a copy of the GNU Affero General Public License
*    along with this program.  If not, see <http://www.gnu.org/licenses/>.
**************************************************************************/


#include "Client.h"
#include "../UIConnector.h"
#include "../settingsValues.h"
#include "../SarahPacket.h"

Client::Client() : msgHandler(this) {
    manager->getAvailablePlugins(plugins);
};

void Client::operator()() {
    if (identify()) {
        disconnect();
        return;
    }
    if (authenticate()) {
        disconnect();
        return;
    }
    std::string message;
    while (pipe->Read(&message)) {
        Json::Reader reader;
        Json::Value root;
        reader.parse(message, root);
        if (root.isObject()) {
            if (!root["type"].isNull()) {
                std::string type = root["type"].asString();
                if (!type.empty()){
                    Json::Value obj;
                    Json::Value v;
                    reader.parse(message,v);
                    obj["type"]= "Client";
                    obj["Device"]= Json::Value(serializeToJson().stringify(false));
                    obj["msg"] = v;
                    ISarahPacket *packet = new SarahPacket(obj);
                    manager->Log("Printing Packet");
                    manager->Log(packet->toString());
                    {
                        for(auto &plugin : plugins){
                            for(auto &packetType:plugin->getHandledPacketTypes()){
                                if(type==packetType){
                                    plugin->onPacketReceived(packet);
                                }
                            }
                        }
                    }
                }
            } else {
                manager->Log("JSON type is Null from Client : " + id + ", Disconnecting", LL_ERROR);
                break;
            }
#ifdef _DEBUG
            if (getID().length() > 0) {
                manager->Log("Client " + getName() + "'s Authentication is " + std::to_string(isAuthenticated), LL_DEBUG);
            }
#endif // DEBUG
        } else {
            manager->Log("Invalid JSON type from Client : " + id + ", Disconnecting", LL_ERROR);
            break;
        }
    }
    disconnect();
}


std::string Client::getName() {
    return this->name;
}

JSON::Object Client::serializeToJson() {
    JSON::Object obj;
    obj.set("id", getID());
    obj.set("name", getName());
    obj.set("os", getOperatingSystem());
    return obj;
}


void Client::RevokRandomKey() {
    char pwd[8];
    manager->getRandonmString(pwd, 8);
    this->authKey.key = std::string(pwd);
    manager->Log("Key for Client " + getName() + " is : " + std::string(pwd), LL_DEBUG);
    msgHandler.HandleAuthChanged();
}


std::string Client::getOperatingSystem() {
    return this->os;
}

int Client::identify() {
    try {
        std::string message;
        if (pipe->Read(&message)) {
            Json::Reader reader;
            Json::Value root;
            reader.parse(message, root);
            if (!root["type"].isNull()) {
                std::string type = root["type"].asString();
                if (type == "ID") {
                    this->id = root["ID"].asString();
                    this->name = root["name"].asString();
                    this->os = root["os"].asString();
                    msgHandler.HandleID();
                    return 0;
                }
            }
        }
    } catch (...) {
        return 1;
    }
    return 1;
}

int Client::authenticate() {
    if (this->isAuthenticated)
        return 0;
#ifdef AUTHENTICATE_WITHOUT_UI
    if (!manager->getUIConnector()->getUIClient()) {
        this->setIsAuthenticated(true);
        manager->wait(500);
        msgHandler.AuthenticationSucceeded();
        return 0;
    }
#endif
    try {
        msgHandler.AuthenticationRequired();
        std::string message;
        for (int i = 0; i < AUTH_REPEAT_TIME; ++i) {
            RevokRandomKey();
            if (pipe->Read(&message)) {
                Json::Reader reader;
                Json::Value root;
                reader.parse(message, root);
                if (!root["type"].isNull()) {
                    std::string type = root["type"].asString();
                    if (type == "Authentication") {
                        std::string code = root["key"].asString();
                        if (code == this->authKey.key) {
                            this->setIsAuthenticated(true);
                            msgHandler.AuthenticationSucceeded();
                            manager->Log(this->getName() + " Authenticated");
                            return 0;
                        } else if (i + 1 < AUTH_REPEAT_TIME) msgHandler.AuthenticationFailed();
                    }
                }
            }
        }
    } catch (...) {
        return 1;
    }
    return 1;
}

