/*************************************************************************
*    Sarah - Desktop Partner
*    Copyright (C) 2019-2019 Ebrahim Karimi
*
*    This program is free software: you can redistribute it and/or modify
*    it under the terms of the GNU Affero General Public License as published by
*    the Free Software Foundation, either version 3 of the License, or
*    (at your option) any later version.
*
*    This program is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU Affero General Public License for more details.
*
*    You should have received a copy of the GNU Affero General Public License
*    along with this program.  If not, see <http://www.gnu.org/licenses/>.
**************************************************************************/


#ifndef SARAH_CLIENT_H
#define SARAH_CLIENT_H

#include "../Abstract/Client.h"


#include "ClientMessageHandler.h"
#include "../Interface/Plugin.h"


class Client : public AClient {

public :
    Client();

    void operator()() override;

    std::string getOperatingSystem();

    std::string getName();

    JSON::Object serializeToJson() override;


private:
    std::string name;
    std::string os;
    ClientMessageHandler msgHandler;
    std::vector<IPlugin*> plugins;

    void RevokRandomKey();

    int identify();

    int authenticate();
};


#endif //SARAH_CLIENT_H
