#pragma once

/*************************************************************************
*    Sarah - Desktop Partner
*    Copyright (C) 2019-2019 Ebrahim Karimi
*
*    This program is free software: you can redistribute it and/or modify
*    it under the terms of the GNU Affero General Public License as published by
*    the Free Software Foundation, either version 3 of the License, or
*    (at your option) any later version.
*
*    This program is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU Affero General Public License for more details.
*
*    You should have received a copy of the GNU Affero General Public License
*    along with this program.  If not, see <http://www.gnu.org/licenses/>.
**************************************************************************/


#ifndef SARAH_CLIENTS_MANAGER_H
#define SARAH_CLIENTS_MANAGER_H

#include "../Abstract/Client.h"
#include <string>
#include <map>
#include <iostream>


class ClientManager : public ISarahObject {

public:
	AClient* getClient(std::string id) {

		std::map<std::string, AClient*>::iterator it;
		it = this->clients.find(id);
		if (it != clients.end())
			return it->second;
		return NULL;
	};

	void addClient(std::string id, AClient* c) {

		if (getClient(id) == NULL) {
			clients[id] = c;
		}
	};

	void removeClient(std::string id) {

		std::map<std::string, AClient*>::iterator it;
		it = this->clients.find(id);
		if (it != clients.end())
			clients.erase(it);
	};

private:
	std::map<std::string, AClient*> clients;
};



#endif // !SARAH_CLIENTS_MANAGER_H
