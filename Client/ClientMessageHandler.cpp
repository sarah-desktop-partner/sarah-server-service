/*************************************************************************
*    Sarah - Desktop Partner
*    Copyright (C) 2019-2019 Ebrahim Karimi
*
*    This program is free software: you can redistribute it and/or modify
*    it under the terms of the GNU Affero General Public License as published by
*    the Free Software Foundation, either version 3 of the License, or
*    (at your option) any later version.
*
*    This program is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU Affero General Public License for more details.
*
*    You should have received a copy of the GNU Affero General Public License
*    along with this program.  If not, see <http://www.gnu.org/licenses/>.
**************************************************************************/



#include "ClientMessageHandler.h"
#include "Client.h"
#include "../settingsValues.h"
#include "../UIConnector.h"

void ClientMessageHandler::HandleID()
{
	JSON::Object o;
	o.set("type", "ID");
	// TODO
	o.set("ID", manager->getUniqueID());
	std::string hostname;
	manager->getHostname(hostname);
	o.set("name", hostname);
	o.set("os", "Linux ubuntu");
	o.set("version", SARAH_VERSION);
	client->Write(o, true, false);
}

void ClientMessageHandler::HandleAuthChanged()
{
    // TODO
    manager->getUIConnector()->Write(client->getKey().key,true);
}

