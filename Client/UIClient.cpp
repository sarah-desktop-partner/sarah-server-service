/*************************************************************************
*    Sarah - Desktop Partner
*    Copyright (C) 2019-2019 Ebrahim Karimi
*
*    This program is free software: you can redistribute it and/or modify
*    it under the terms of the GNU Affero General Public License as published by
*    the Free Software Foundation, either version 3 of the License, or
*    (at your option) any later version.
*
*    This program is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU Affero General Public License for more details.
*
*    You should have received a copy of the GNU Affero General Public License
*    along with this program.  If not, see <http://www.gnu.org/licenses/>.
**************************************************************************/


#include "UIClient.h"
#include "../json/json.h"
#include "../UIConnector.h"

UiClient::UiClient() : msgHandler(this)
{
	authKey.key = manager->getUIPassword();
	authKey.time = -1;
	manager->Log("UI Password is : " + authKey.key, LL_DEBUG);
}

void UiClient::operator()()
{
	std::string message;
	while (pipe->Read(&message))
	{
		Json::Reader reader;
		Json::Value root;
		reader.parse(message, root);
		if (root.isObject()) {
			if (!root["type"].isNull()) {
				std::string type = root["type"].asString();
				if (type == "Authenticate") {
					if (!root["key"].isNull()) {
						std::string arrivedKey = root["key"].asString();
						if (arrivedKey == this->authKey.key)
						{
							this->setIsAuthenticated(true);
							msgHandler.AuthenticationSucceeded();
							manager->getUIConnector()->setUIClient(this);
						}
						else {
							manager->Log("Authentication Filed for UI Client", LL_INFO);
							this->setIsAuthenticated(false);
							manager->getUIConnector()->setUIClient(nullptr);
							msgHandler.AuthenticationFailed();
						}
					}
					else {
						manager->Log("Couldn't Collect Key from Authentication Message in UI Client", LL_ERROR);
						msgHandler.AuthenticationFailed();
					}
				}
			}else {
				manager->Log("JSON type is Null from UI Client, Disconnecting", LL_ERROR);
				break;
			}
		}else {
			manager->Log("Invalid JSON type from UI Client, Disconnecting", LL_ERROR);
			break;
		}
	}

	manager->getUIConnector()->setUIClient(nullptr);
	disconnect();
}
