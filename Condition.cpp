/*************************************************************************
*    Sarah - Desktop Partner
*    Copyright (C) 2019-2019 Ebrahim Karimi
*
*    This program is free software: you can redistribute it and/or modify
*    it under the terms of the GNU Affero General Public License as published by
*    the Free Software Foundation, either version 3 of the License, or
*    (at your option) any later version.
*
*    This program is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU Affero General Public License for more details.
*
*    You should have received a copy of the GNU Affero General Public License
*    along with this program.  If not, see <http://www.gnu.org/licenses/>.
**************************************************************************/


#include "Condition.h"
#include "Mutex.h"

void SCondition::wait(IScopedLock *lock, int timems) {
    std::unique_lock<std::recursive_mutex> *tl=((SLock*)lock->getLock())->getLock();
    if (timems < 0)
    {
        cond.wait(*tl);
    }
    else
    {
        cond.wait_for(*tl, std::chrono::milliseconds(timems));
    }
}

void SCondition::notify_one() {
    cond.notify_one();
}

void SCondition::notify_all() {
    cond.notify_all();
}
