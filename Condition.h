/*************************************************************************
*    Sarah - Desktop Partner
*    Copyright (C) 2019-2019 Ebrahim Karimi
*
*    This program is free software: you can redistribute it and/or modify
*    it under the terms of the GNU Affero General Public License as published by
*    the Free Software Foundation, either version 3 of the License, or
*    (at your option) any later version.
*
*    This program is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU Affero General Public License for more details.
*
*    You should have received a copy of the GNU Affero General Public License
*    along with this program.  If not, see <http://www.gnu.org/licenses/>.
**************************************************************************/


#ifndef SARAH_CONDITION_H
#define SARAH_CONDITION_H


#include <condition_variable>
#include "Interface/Condition.h"

class SCondition : public ICondition {
public:
    void wait(IScopedLock *lock, int timems = -1) override;

    void notify_one() override;

    void notify_all() override;

private:
    std::condition_variable_any cond;
};


#endif //SARAH_CONDITION_H
