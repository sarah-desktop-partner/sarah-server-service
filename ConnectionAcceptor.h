/*************************************************************************
*    Sarah - Desktop Partner
*    Copyright (C) 2019-2019 Ebrahim Karimi
*
*    This program is free software: you can redistribute it and/or modify
*    it under the terms of the GNU Affero General Public License as published by
*    the Free Software Foundation, either version 3 of the License, or
*    (at your option) any later version.
*
*    This program is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU Affero General Public License for more details.
*
*    You should have received a copy of the GNU Affero General Public License
*    along with this program.  If not, see <http://www.gnu.org/licenses/>.
**************************************************************************/


#ifndef SARAH_CONNECTIONACCEPTOR_H
#define SARAH_CONNECTIONACCEPTOR_H



#include "Interface/Thread.h"
#include "Interface/Client.h"

#include <type_traits>

template <class T>
class ConnectionAcceptor : public IThread {
public:
    explicit ConnectionAcceptor(unsigned short int port,const std::string& server_name,bool single=false);
    ~ConnectionAcceptor() override;
    void operator()() override;
    bool hasError();

private:
    SOCKET s;
    bool error;
    bool singleShot;
    std::string name;
};

#include "ConnectionAcceptor.h"
#include "Client/Client.h"
#include <string>
#include <cstring>
#include "socket_header.h"
#include "Interface/Manager.h"


template<class T>
ConnectionAcceptor<T>::ConnectionAcceptor(unsigned short int port,const std::string& server_name, bool single) : error(false), singleShot(single) {

    int type = SOCK_STREAM;
    this->name = server_name;
#if !defined(_WIN32) && defined(SOCK_CLOEXEC)
    type |= SOCK_CLOEXEC;
#endif
    s = socket(AF_INET, type, 0);
    if (s < 1) {
        manager->Log("Creating SOCKET failed. Port " + std::to_string((int) port) + " may already be in use", LL_ERROR);
        error = true;
        return;
    }
#if !defined(_WIN32) && !defined(SOCK_CLOEXEC)
    fcntl(s, F_SETFD, fcntl(s, F_GETFD, 0) | FD_CLOEXEC);
#endif

    int optval = 1;
    int rc = setsockopt(s, SOL_SOCKET, SO_REUSEADDR, (char *) &optval, sizeof(int));

    if (rc == SOCKET_ERROR) {
        manager->Log("Failed setting SO_REUSEADDR for port " + std::to_string(port), LL_ERROR);
        error = true;
        return;
    }
#ifdef __APPLE__
    optval = 1;
    setsockopt(s, SOL_SOCKET, SO_NOSIGPIPE, (void*)&optval, sizeof(optval));
#endif

    sockaddr_in addr{};

    memset(&addr, 0, sizeof(sockaddr_in));
    addr.sin_family = AF_INET;
    addr.sin_port = htons(port);
    addr.sin_addr.s_addr = htonl(INADDR_ANY);

    rc = bind(s, (sockaddr *) &addr, sizeof(addr));
    if (rc == SOCKET_ERROR) {
        manager->Log("Failed binding SOCKET to Port " + std::to_string(port), LL_ERROR);
        error = true;
        return;
    }

    listen(s, 10000);
    manager->Log("Started " + server_name + " Server Successfully!");
    manager->Log("Server " + server_name + " Listening on Port : " + std::to_string(port));

}

template<class T>
void ConnectionAcceptor<T>::operator()() {
    do {
        socklen_t addrsize = sizeof(sockaddr_in);

        sockaddr_in naddr{};
        SOCKET ns = accept(s, (sockaddr *) &naddr, &addrsize);
        if (ns != SOCKET_ERROR) {
			

#ifdef __APPLE__
            int optval = 1;
            setsockopt(ns, SOL_SOCKET, SO_NOSIGPIPE, (void*)&optval, sizeof(optval));
#endif

            IClient *c=new T();
            c->init(ns);
			char r[15];
			manager->getRandonmString(r, sizeof(15));
            manager->createThread(c,std::string(r));
            manager->Log("New Incoming Connection to " + this->name, LL_INFO);
        } else {
            manager->Log("Accepting client failed to Server " + this->name, LL_ERROR);
            manager->wait(1000);
        }

    } while (!singleShot);
}

template<class T>
ConnectionAcceptor<T>::~ConnectionAcceptor() {
    closesocket(s);
}
template <class T>
bool ConnectionAcceptor<T>::hasError(){
    return error;
}



#endif //SARAH_CONNECTIONACCEPTOR_H
