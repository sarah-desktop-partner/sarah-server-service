/*************************************************************************
*    Sarah - Desktop Partner
*    Copyright (C) 2019-2019 Ebrahim Karimi
*
*    This program is free software: you can redistribute it and/or modify
*    it under the terms of the GNU Affero General Public License as published by
*    the Free Software Foundation, either version 3 of the License, or
*    (at your option) any later version.
*
*    This program is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU Affero General Public License for more details.
*
*    You should have received a copy of the GNU Affero General Public License
*    along with this program.  If not, see <http://www.gnu.org/licenses/>.
**************************************************************************/


#include "FileLogger.h"
#include "Exception/IOFError.h"
#include <ctime>

FileLogger::FileLogger(const std::string& filepath,LOG_LEVEL log_level) {
    log_stream.open(filepath,std::ios::out | std::ios::app | std::ios::binary);
    if (!log_stream.is_open())
    {
        throw IOFError("Couldn't open file");
    }
    setLogLevel(log_level);
}

void FileLogger::Log(const std::string &msg, LOG_LEVEL log_level) {
    if(log_level>=getLogLevel())
    {
        log_stream<<msg.c_str()<<std::endl;
        log_stream.flush();
    }
}