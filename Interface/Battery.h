/*************************************************************************
*    Sarah - Desktop Partner
*    Copyright (C) 2019-2019 Ebrahim Karimi
*
*    This program is free software: you can redistribute it and/or modify
*    it under the terms of the GNU Affero General Public License as published by
*    the Free Software Foundation, either version 3 of the License, or
*    (at your option) any later version.
*
*    This program is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU Affero General Public License for more details.
*
*    You should have received a copy of the GNU Affero General Public License
*    along with this program.  If not, see <http://www.gnu.org/licenses/>.
**************************************************************************/


#ifndef SARAH_BATTERY_H
#define SARAH_BATTERY_H


#include <string>
#include "Object.h"
#include "../json/jsonUrbackup.h"

struct BatteryStatus{
    bool batterySaving;
    bool isConnectedToAC;
    short batteryPercentage;
    std::string status;
};
class Battery : public ISarahObject
{
public:
    static bool getBatteryStatus(BatteryStatus &);
    static JSON::Object serializeToJson(const BatteryStatus& bt);
};
#endif //SARAH_BATTERY_H
