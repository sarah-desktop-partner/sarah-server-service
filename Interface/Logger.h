/*************************************************************************
*    Sarah - Desktop Partner
*    Copyright (C) 2019-2019 Ebrahim Karimi
*
*    This program is free software: you can redistribute it and/or modify
*    it under the terms of the GNU Affero General Public License as published by
*    the Free Software Foundation, either version 3 of the License, or
*    (at your option) any later version.
*
*    This program is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU Affero General Public License for more details.
*
*    You should have received a copy of the GNU Affero General Public License
*    along with this program.  If not, see <http://www.gnu.org/licenses/>.
**************************************************************************/


#ifndef SARAH_LOGGER_H
#define SARAH_LOGGER_H

#include "Object.h"
#include <string>

enum LOG_LEVEL{
    LL_DEBUG=0,
    LL_INFO,
    LL_WARN,
    LL_ERROR
};


class ILogger : public ISarahObject
{
public:
    virtual void Log(const std::string& msg,LOG_LEVEL log_level=LL_INFO)=0;
};
#endif //SARAH_LOGGER_H
