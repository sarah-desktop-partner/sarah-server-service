/*************************************************************************
*    Sarah - Desktop Partner
*    Copyright (C) 2019-2019 Ebrahim Karimi
*
*    This program is free software: you can redistribute it and/or modify
*    it under the terms of the GNU Affero General Public License as published by
*    the Free Software Foundation, either version 3 of the License, or
*    (at your option) any later version.
*
*    This program is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU Affero General Public License for more details.
*
*    You should have received a copy of the GNU Affero General Public License
*    along with this program.  If not, see <http://www.gnu.org/licenses/>.
**************************************************************************/


#ifndef SARAH_IMANAGER_H
#define SARAH_IMANAGER_H

#include <map>
#include "Logger.h"
#include "Plugin.h"
#include "PluginManager.h"


class IThread;
class UIConnector;
class ClientManager;

class IManager : public ILogger
{
public:
    virtual bool createThread(IThread *thd_obj, const std::string& name)=0;
    virtual bool addLogger(ILogger* logger)=0;
    virtual void wait(unsigned int ms) =0 ;

    virtual void LogError(int rc,const std::string& msg,bool dump=false)=0;

    virtual void getRandonmString(char *s, int size) =0;

    virtual bool getHostname(std::string &)=0;

	virtual bool setWorkingDirectory(const std::string& path)=0;

	virtual void setParameters(const std::map<std::string,std::string>& parameters)=0;

	virtual std::map<std::string,std::string>& getParameters()=0;

	virtual bool getAvailablePlugins(std::vector<IPlugin *> &plugins)=0;

	virtual bool registerPlugin(IPluginManager *, PLUGIN_ID)=0;

	virtual std::string getUIPassword() = 0;

	virtual UIConnector *getUIConnector()=0;

	virtual ClientManager *getClientManager()=0;

    virtual std::string getUniqueID()=0;

    virtual bool LoadDLL(const std::string& name)=0;
};
#endif //SARAH_IMANAGER_H
