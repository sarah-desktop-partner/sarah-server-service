/*************************************************************************
*    Sarah - Desktop Partner
*    Copyright (C) 2019-2019 Ebrahim Karimi
*
*    This program is free software: you can redistribute it and/or modify
*    it under the terms of the GNU Affero General Public License as published by
*    the Free Software Foundation, either version 3 of the License, or
*    (at your option) any later version.
*
*    This program is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU Affero General Public License for more details.
*
*    You should have received a copy of the GNU Affero General Public License
*    along with this program.  If not, see <http://www.gnu.org/licenses/>.
**************************************************************************/


#ifndef SARAH_IMUTEX_H
#define SARAH_IMUTEX_H

#include "Object.h"
#include "Types.h"

class ILock : public ISarahObject {
};

class IMutex : public ISarahObject {
public:
    virtual void Lock() = 0;

    virtual ILock * Lock2()=0;
    virtual void Unlock() = 0;
    virtual bool TryLock()=0;
};

class IScopedLock
{
public:
    IScopedLock(IMutex *pMutex){ if(pMutex!=NULL)lock=pMutex->Lock2();else lock=NULL; }
    ~IScopedLock(){ if(lock!=NULL) lock->Remove(); }
    void relock(IMutex *pMutex){ if(lock!=NULL) lock->Remove(); if(pMutex!=NULL)lock=pMutex->Lock2();else lock=NULL; }
    ILock * getLock(){ return lock; }

private:
    ILock *lock;
};

#endif //SARAH_IMUTEX_H
