/*************************************************************************
*    Sarah - Desktop Partner
*    Copyright (C) 2019-2019 Ebrahim Karimi
*
*    This program is free software: you can redistribute it and/or modify
*    it under the terms of the GNU Affero General Public License as published by
*    the Free Software Foundation, either version 3 of the License, or
*    (at your option) any later version.
*
*    This program is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU Affero General Public License for more details.
*
*    You should have received a copy of the GNU Affero General Public License
*    along with this program.  If not, see <http://www.gnu.org/licenses/>.
**************************************************************************/


#ifndef SARAH_IPIPE_H
#define SARAH_IPIPE_H

#include "Object.h"
#include <string>

#ifdef _WIN32

#include <WinSock2.h>

#else

#include "../socket_header.h"

#endif

#include "../libs.h"
#include "../json/jsonUrbackup.h"
#include "WritablePipe.h"
#include "ReadablePipe.h"

class IPipe : public IWritablePipe, public IReadablePipe {
public:

    virtual size_t Read(char *buffer, size_t bsize) = 0;

    virtual size_t Read(std::string *ret) = 0;

    virtual bool Write(const char *buffer, size_t bsize, bool flush) = 0;

    virtual bool Write(JSON::Object obj, bool compressed = true, bool flush = true) = 0;

    virtual bool Write(const std::string &str, bool flush) = 0;

    virtual bool hasError() = 0;

    virtual SOCKET getSocket() = 0;

    void Remove() override { delete this; }
};

#endif //SARAH_PIPE_H
