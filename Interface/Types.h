/*************************************************************************
*    Sarah - Desktop Partner
*    Copyright (C) 2019-2019 Ebrahim Karimi
*
*    This program is free software: you can redistribute it and/or modify
*    it under the terms of the GNU Affero General Public License as published by
*    the Free Software Foundation, either version 3 of the License, or
*    (at your option) any later version.
*
*    This program is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU Affero General Public License for more details.
*
*    You should have received a copy of the GNU Affero General Public License
*    along with this program.  If not, see <http://www.gnu.org/licenses/>.
**************************************************************************/


#ifndef SARAH_TYPES_H
#define SARAH_TYPES_H
#ifndef NULL
#ifdef _WIN32
#define NULL nullptr
#else
#define NULL 0
#endif
#endif

#ifdef LINUX
typedef long long int __int64;
typedef unsigned long long int uint64;
#elif _WIN32
typedef unsigned __int64 uint64;
#else
typedef long long int __int64;
typedef unsigned long long int uint64;
#endif

typedef __int64 int64;
typedef unsigned char uchar;
typedef int _i32;
typedef __int64 _i64;
typedef unsigned int _u32;
typedef unsigned short _u16;
typedef short _i16;
typedef int PLUGIN_ID;

#ifdef _WIN32
#include<WinSock2.h>
#include <windows.h>
#else
typedef void *HMODULE;
#endif

#endif //SARAH_TYPES_H
