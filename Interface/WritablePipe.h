/*************************************************************************
*    Sarah - Desktop Partner
*    Copyright (C) 2019-2019 Ebrahim Karimi
*
*    This program is free software: you can redistribute it and/or modify
*    it under the terms of the GNU Affero General Public License as published by
*    the Free Software Foundation, either version 3 of the License, or
*    (at your option) any later version.
*
*    This program is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU Affero General Public License for more details.
*
*    You should have received a copy of the GNU Affero General Public License
*    along with this program.  If not, see <http://www.gnu.org/licenses/>.
**************************************************************************/


#ifndef SARAH_IWRITABLEPIPE_H
#define SARAH_IWRITABLEPIPE_H

#include "Object.h"
#include "../json/jsonUrbackup.h"

class IWritablePipe : public ISarahObject
{

    virtual bool Write(const char *buffer, size_t bsize, bool flush) = 0;

    virtual bool Write(JSON::Object obj, bool compressed=true, bool flush=true) = 0;

    virtual bool Write(const std::string &str, bool flush) = 0;

};
#endif //SARAH_IWRITABLEPIPE_H
