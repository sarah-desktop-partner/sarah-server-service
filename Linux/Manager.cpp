/*************************************************************************
*    Sarah - Desktop Partner
*    Copyright (C) 2019-2019 Ebrahim Karimi
*
*    This program is free software: you can redistribute it and/or modify
*    it under the terms of the GNU Affero General Public License as published by
*    the Free Software Foundation, either version 3 of the License, or
*    (at your option) any later version.
*
*    This program is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU Affero General Public License for more details.
*
*    You should have received a copy of the GNU Affero General Public License
*    along with this program.  If not, see <http://www.gnu.org/licenses/>.
**************************************************************************/


#include <string>
#include <dlfcn.h>
#include "../Manager.h"

bool Manager::LoadDLL(const std::string &name) {

    int mode=RTLD_NOW | RTLD_GLOBAL;
    HMODULE dll = dlopen( name.c_str(), mode);

    if(dll==NULL)
    {
        Log("DLL not found: "+(std::string)dlerror(), LL_ERROR);
        return false;
    }

    unload_handles.push_back( dll );

    LOADACTIONS load_func=NULL;
    load_func=(LOADACTIONS)dlsym(dll,"LoadActions");
    UNLOADACTIONS unload_function = NULL;
    unload_function=(UNLOADACTIONS) dlsym(dll,"UnloadActions");

    if(load_func==NULL)
    {
        Log("Loading function in DLL not found", LL_ERROR);
        return false;
    }
    if(unload_function==NULL)
    {
        Log("UnLoading function in DLL not found", LL_ERROR);
        return false;
    }

    load_func(this);
    unload_functions.insert(std::pair<std::string, UNLOADACTIONS>(name,unload_function) );
    manager->Log("Loaded Plugin " + name,LL_DEBUG);
    return true;
}
