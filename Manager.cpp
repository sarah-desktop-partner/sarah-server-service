/*************************************************************************
*    Sarah - Desktop Partner
*    Copyright (C) 2019-2019 Ebrahim Karimi
*
*    This program is free software: you can redistribute it and/or modify
*    it under the terms of the GNU Affero General Public License as published by
*    the Free Software Foundation, either version 3 of the License, or
*    (at your option) any later version.
*
*    This program is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU Affero General Public License for more details.
*
*    You should have received a copy of the GNU Affero General Public License
*    along with this program.  If not, see <http://www.gnu.org/licenses/>.
**************************************************************************/


#ifndef _WIN32

#include <zconf.h>

#else

#include <WinSock2.h>
#include <Windows.h>
#include <processthreadsapi.h>
#include <direct.h>
#endif

#include <random>
#include <iostream>
#include <fstream>
#include "Manager.h"
#include "Mutex.h"
#include "Interface/SarahFile.h"
#include "settingsValues.h"

#ifdef _WIN32
struct SThreadInfo
{
    std::string name;
    IThread* t;
};

#ifdef _WIN32
//from https://msdn.microsoft.com/en-us/library/xcb2z8hs.aspx
const DWORD MS_VC_EXCEPTION = 0x406D1388;
#pragma pack(push,8)
typedef struct tagTHREADNAME_INFO
{
    DWORD dwType; // Must be 0x1000.
    LPCSTR szName; // Pointer to name (in user addr space).
    DWORD dwThreadID; // Thread ID (-1=caller thread).
    DWORD dwFlags; // Reserved for future use, must be zero.
} THREADNAME_INFO;
#pragma pack(pop)
void SetThreadName(DWORD dwThreadID, const char* threadName) {
    THREADNAME_INFO info;
    info.dwType = 0x1000;
    info.szName = threadName;
    info.dwThreadID = dwThreadID;
    info.dwFlags = 0;
#pragma warning(push)
#pragma warning(disable: 6320 6322)
    __try {
            RaiseException(MS_VC_EXCEPTION, 0, sizeof(info) / sizeof(ULONG_PTR), (ULONG_PTR*)&info);
    }
    __except (EXCEPTION_EXECUTE_HANDLER) {
    }
#pragma warning(pop)
}
#endif

DWORD WINAPI thread_helper_f(LPVOID param)
{
    IThread* t;
#if defined(_WIN32) && defined(_DEBUG)
    SThreadInfo* thread_info = reinterpret_cast<SThreadInfo*>(param);
    SetThreadName(-1,thread_info->name.c_str());
    t = thread_info->t;
    delete thread_info;
#else
    t = reinterpret_cast<IThread*>(param);
#endif
    (*t)();
    return 0;
}
#else //_WIN32

void *thread_helper_f(void *t) {
    auto *tmp = (IThread *) t;
    (*tmp)();
    return NULL;
}

#endif //_WIN32

Manager::Manager() : clientsManager(new ClientManager()), plugins_mutex(new SMutex()), uic(new UIConnector()),
                     loggers_mutex(new SMutex()) {

}

bool Manager::createThread(IThread *thd_obj, const std::string &name) {

    const size_t thread_large_stack_size = 64 * 1024 * 1024;

#ifdef _WIN32
    void* param;
#if defined(_WIN32) && defined(_DEBUG)
    SThreadInfo* thread_info = new SThreadInfo;
    thread_info->name = name;
    thread_info->t = thd_obj;
    param = thread_info;
#else
    param = thd_obj;
#endif
        HANDLE hThread = CreateThread(NULL, 0, &thread_helper_f, param, 0, NULL);

    if (hThread == NULL)
    {
        manager->Log("Error creating thread. Errno: " + std::to_string(GetLastError()), LL_ERROR);
        return false;
    }
    else
    {
        CloseHandle(hThread);
        return true;
    }
#else
    pthread_attr_t attr;
    if (pthread_attr_init(&attr) != 0) {
        manager->Log("Error initializing pthread attrs", LL_ERROR);
        return false;
    }
#if !defined(URB_THREAD_STACKSIZE64) || !defined(URB_THREAD_STACKSIZE32)

#ifndef _LP64
    //Only on 32bit architectures
    pthread_attr_setstacksize(&attr, 1*1024*1024);
#endif

#else

#ifdef _LP64
    pthread_attr_setstacksize(&attr, (URB_THREAD_STACKSIZE64));
#else
    pthread_attr_setstacksize(&attr, (URB_THREAD_STACKSIZE32));
#endif

#endif

    if (pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_DETACHED) != 0) {
        manager->Log("Error setting thread to detached", LL_ERROR);
        pthread_attr_destroy(&attr);
        return false;
    }

    pthread_t t;
    if (pthread_create(&t, &attr, &thread_helper_f, (void *) thd_obj) != 0) {
        manager->Log("Error creating pthread. Errno: " + std::to_string(errno), LL_ERROR);
        pthread_attr_destroy(&attr);
        return false;
    }


#ifdef HAVE_PTHREAD_SETNAME_NP
    if (!name.empty())
    {
        std::string thread_name;
        if (name.size() > 15)
        {
            thread_name = name.substr(0, 15);
        }
        else
        {
            thread_name = name;
        }

        pthread_setname_np(t, thread_name.c_str());
    }
#endif //HAVE_PTHREAD_SETNAME_NP

    pthread_attr_destroy(&attr);
    return true;
#endif
}

void Manager::Log(const std::string &msg, LOG_LEVEL log_level) {
    IScopedLock lock(loggers_mutex);
    std::string finalMessage;
    time_t rawtime;
    struct tm *timeinfo;
    char buffer[100];
    time(&rawtime);
    timeinfo = localtime(&rawtime);
    strftime(buffer, 100, "%Y-%m-%d %X: ", timeinfo);
    finalMessage.append(buffer);
    switch (log_level) {
        case LL_DEBUG:
            finalMessage.append("DEBUG : ");
            break;
        case LL_INFO:
            finalMessage.append("INFO  : ");
            break;
        case LL_WARN:
            finalMessage.append("WARN  : ");
            break;
        case LL_ERROR:
            finalMessage.append("ERROR : ");
            break;
    }
    finalMessage.append(msg).append(SarahFile::getLineBreaker());

    for (ILogger *l : loggers) {
        l->Log(finalMessage, log_level);
    }
    std::cout << finalMessage;
}

bool Manager::addLogger(ILogger *logger) {
    try {
        IScopedLock lock(loggers_mutex);
        loggers.emplace_back(logger);
        return true;
    } catch (...) {
        return false;
    }

}

void Manager::wait(unsigned int ms) {
#ifdef _WIN32
    Sleep(ms);
#else
    usleep(ms * 1000);
#endif
}

void Manager::getRandonmString(char *s, int size) {
    static const char alphanum[] =
            "0123456789"
            "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
            "abcdefghijklmnopqrstuvwxyz";
#ifdef _WIN32
    std::random_device rd;
    std::mt19937 mt(rd());

    std::uniform_int_distribution<int> dist(0,size);

#endif
    for (int i = 0; i < size; ++i) {
#ifndef _WIN32
        s[i] = alphanum[rand() % (sizeof(alphanum) - 1)];
#else
        s[i] = alphanum[dist(mt)];
#endif
    }
    s[size - 1] = 0;
}

bool Manager::getHostname(std::string &hostname) {
    int rc;
    char buff[256];
    rc = gethostname(buff, 256);
    hostname = std::string(buff);
    return rc == 0;
}

void Manager::LogError(int rc, const std::string &msg, bool dump) {
    std::string error;
#ifdef _WIN32
    switch (rc)
    {
        case S_OK:
            error = "OK";
            break;
        case E_POINTER:
            error = "E_POINTER";
            break;
        case CO_E_NOTINITIALIZED:
            error = "CO_E_NOTINITIALIZED";
            break;
        case E_ACCESSDENIED:
            error = "E_ACCESSDENIED";
            break;
        case E_NOINTERFACE:
            error = "E_NOINTERFACE";
            break;
        case E_OUTOFMEMORY:
            error = "E_OUTOFMEMORY";
            break;
        case E_FAIL:
            error = "E_FAIL";
            break;
        case E_UNEXPECTED:
            error = "E_UNEXPECTED";
            break;
        case E_HANDLE:
            error = "E_HANDLE";
            break;
        case E_ABORT:
            error = "E_ABORT";
            break;
        default:
            error = "Unknown";
    }
#endif
    Log("Error Occurred : " + error, LL_ERROR);
    Log(msg, LL_ERROR);
    if (dump)
        exit(rc);
}

bool Manager::setWorkingDirectory(const std::string &path) {
    int rc = 0;
#ifdef _WIN32
    std::wstring stemp = std::wstring(path.begin(), path.end());
    rc=SetCurrentDirectory(stemp.c_str());
	if(rc==0){
		LogError(rc, "setWorkingDirectory", false);
	}
    return rc != 0;
#else
    rc = chdir(path.c_str());
    return rc == 0;
#endif // _WIN32
}

std::string Manager::getUIPasswordLocation() {
    std::string cwd;
    char pwdc[1024];
#ifdef _WIN32
    _getcwd(pwdc, sizeof(pwdc));
    cwd = std::string(pwdc);
#else
    getcwd(pwdc, sizeof(pwdc));
    cwd = std::string(pwdc);
#endif // _WIN32

    return cwd + SarahFile::getFileSeperator() + UI_PASSWD_FILE_NAME;
}

std::string Manager::getUIPassword() {
    char pwdChars[1024];
    std::string pwd = "feRb3G$rw";
    std::string cwd = getUIPasswordLocation();
    std::ifstream pwdFile(cwd, std::ios::in);
    if (pwdFile.is_open()) {
        pwdFile.read(pwdChars, sizeof(pwdChars));
        pwd = std::string(pwdChars);
        pwdFile.close();
    }
    return pwd;
}

UIConnector *Manager::getUIConnector() {
    return uic;
}

ClientManager *Manager::getClientManager() {
    return clientsManager;
}

std::string Manager::getUniqueID() {
    return this->id;
}

void Manager::setUniqueID(const std::string &id) {
    this->id = id;
}


void Manager::setParameters(const std::map<std::string, std::string> &parameters) {
    this->parameters = parameters;
}

std::map<std::string, std::string> &Manager::getParameters() {
    return this->parameters;
};

bool Manager::getAvailablePlugins(std::vector<IPlugin *> &plugins) {
    std::map<PLUGIN_ID, IPluginManager *>::iterator it;
    it = available_plugins.begin();
    while (it != available_plugins.end()) {
        plugins.push_back(it->second->createPluginInstance(getParameters()));
        it++;
    }
    return true;
}

bool Manager::registerPlugin(IPluginManager *pluginManager, PLUGIN_ID id) {
    IScopedLock lock(plugins_mutex);

    auto iter = available_plugins.find(id);

    if (iter != available_plugins.end())
        return false;

    available_plugins.insert(std::pair<PLUGIN_ID, IPluginManager *>(id, pluginManager));
    return true;
}
