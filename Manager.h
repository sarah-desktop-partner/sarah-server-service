/*************************************************************************
*    Sarah - Desktop Partner
*    Copyright (C) 2019-2019 Ebrahim Karimi
*
*    This program is free software: you can redistribute it and/or modify
*    it under the terms of the GNU Affero General Public License as published by
*    the Free Software Foundation, either version 3 of the License, or
*    (at your option) any later version.
*
*    This program is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU Affero General Public License for more details.
*
*    You should have received a copy of the GNU Affero General Public License
*    along with this program.  If not, see <http://www.gnu.org/licenses/>.
**************************************************************************/


#ifndef SARAH_MANAGER_H
#define SARAH_MANAGER_H

#include "Interface/Manager.h"
#include "Interface/Mutex.h"
#include <vector>
#include "UIConnector.h"
#include "Client/ClientManager.h"
#include "Client/Client.h"
#include "Interface/PluginManager.h"

typedef void(*LOADACTIONS)(IManager *);

typedef void(*UNLOADACTIONS)(void);

class Manager : public IManager {
public:
    Manager();

    bool createThread(IThread *thd_obj, const std::string &name) override;

    void Log(const std::string &msg, LOG_LEVEL log_level) override;

    bool addLogger(ILogger *logger) override;

    void wait(unsigned int ms) override;

    void getRandonmString(char *s, int size) override;

    bool getHostname(std::string &) override;

    void LogError(int rc, const std::string &msg, bool dump = false) override;

    bool setWorkingDirectory(const std::string &path) override;

    void setParameters(const std::map<std::string, std::string> &parameters) override;

    std::map<std::string, std::string> &getParameters() override;

    bool getAvailablePlugins(std::vector<IPlugin *> &plugins) override;

    bool registerPlugin(IPluginManager *, PLUGIN_ID) override;

    std::string getUIPasswordLocation();

    std::string getUIPassword() override;

    UIConnector *getUIConnector() override;


    ClientManager *getClientManager() override;

    std::string getUniqueID() override;

    void setUniqueID(const std::string &);

    bool LoadDLL(const std::string &name) override;

private:
    IMutex *loggers_mutex;

    IMutex *plugins_mutex;

    std::vector<ILogger *> loggers;

    UIConnector *uic;

    ClientManager *clientsManager;

    std::string id;

    std::vector<HMODULE> unload_handles;

    std::map<std::string, UNLOADACTIONS> unload_functions;

    std::map<PLUGIN_ID, IPluginManager *> available_plugins;

    std::map<std::string, std::string> parameters;
};

#endif //SARAH_MANAGER_H
