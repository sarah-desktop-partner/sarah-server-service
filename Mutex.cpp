/*************************************************************************
*    Sarah - Desktop Partner
*    Copyright (C) 2019-2019 Ebrahim Karimi
*
*    This program is free software: you can redistribute it and/or modify
*    it under the terms of the GNU Affero General Public License as published by
*    the Free Software Foundation, either version 3 of the License, or
*    (at your option) any later version.
*
*    This program is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU Affero General Public License for more details.
*
*    You should have received a copy of the GNU Affero General Public License
*    along with this program.  If not, see <http://www.gnu.org/licenses/>.
**************************************************************************/


#include "Mutex.h"

SMutex::SMutex() {
    lock = NULL;
}

SMutex::~SMutex() = default;

void SMutex::Lock() {
    auto *n_lock = new std::unique_lock<std::recursive_mutex>(mutex);
    lock = n_lock;
}

bool SMutex::TryLock() {
    if (mutex.try_lock()) {
        lock = new std::unique_lock<std::recursive_mutex>(mutex, std::adopt_lock);
        return true;
    } else {
        return false;
    }
}

void SMutex::Unlock() {
    delete lock;
}


ILock *SMutex::Lock2() {
    return new SLock(new std::unique_lock<std::recursive_mutex>(mutex));
}


SLock::SLock(std::unique_lock<std::recursive_mutex> *pLock) {
    lock = pLock;
}

SLock::~SLock() {
    delete lock;
}

std::unique_lock<std::recursive_mutex> *SLock::getLock() {
    return lock;
}
