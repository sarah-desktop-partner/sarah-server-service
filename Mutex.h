/*************************************************************************
*    Sarah - Desktop Partner
*    Copyright (C) 2019-2019 Ebrahim Karimi
*
*    This program is free software: you can redistribute it and/or modify
*    it under the terms of the GNU Affero General Public License as published by
*    the Free Software Foundation, either version 3 of the License, or
*    (at your option) any later version.
*
*    This program is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU Affero General Public License for more details.
*
*    You should have received a copy of the GNU Affero General Public License
*    along with this program.  If not, see <http://www.gnu.org/licenses/>.
**************************************************************************/


#ifndef SARAH_MUTEX_H
#define SARAH_MUTEX_H


#include <mutex>
#include "Interface/Mutex.h"

class SMutex : public IMutex {
public:
    SMutex();
    ~SMutex() override;
    void Lock()override ;
    void Unlock()override ;
    ILock* Lock2() override ;
    bool TryLock() override ;

private:
    std::recursive_mutex mutex;
    std::unique_lock<std::recursive_mutex> *lock;
};

class SLock : public ILock
{
public:
    explicit SLock(std::unique_lock<std::recursive_mutex> *pLock);
    ~SLock() override;

    std::unique_lock<std::recursive_mutex>* getLock();
private:

    std::unique_lock<std::recursive_mutex>* lock;
};

#endif //SARAH_MUTEX_H
