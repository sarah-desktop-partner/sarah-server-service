/*************************************************************************
*    Sarah - Desktop Partner
*    Copyright (C) 2019-2019 Ebrahim Karimi
*
*    This program is free software: you can redistribute it and/or modify
*    it under the terms of the GNU Affero General Public License as published by
*    the Free Software Foundation, either version 3 of the License, or
*    (at your option) any later version.
*
*    This program is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU Affero General Public License for more details.
*
*    You should have received a copy of the GNU Affero General Public License
*    along with this program.  If not, see <http://www.gnu.org/licenses/>.
**************************************************************************/



#include "Pipe.h"

#ifndef _WIN32
#include <cstring>
#endif

Pipe::Pipe(SOCKET pSocket) : has_error(false) {
    this->s = pSocket;
}


size_t Pipe::Read(char *buffer, size_t bsize) {
    int rc = recv(s, buffer, (int) bsize, 0);
    if (rc <= 0) {
#ifdef _WIN32
        DWORD err = WSAGetLastError();
        if (err != WSAEWOULDBLOCK && err != WSAEINTR) {
            has_error = true;
        }
#else
        if(errno!=EAGAIN && errno!=EWOULDBLOCK && errno!=EINTR)
        {
            has_error=true;
        }
#endif
        return 0;
    }
    return rc;
}

bool Pipe::Write(const char *buffer, size_t bsize, bool flush) {
    size_t written = 0;
    int rc = send(s, buffer, (int) bsize, 0);
    if (rc >= 0) {
        written += rc;
        if (written < bsize) {
            return Write(buffer + written, bsize - written, flush);
        }
    } else {
#ifdef _WIN32
        DWORD err = WSAGetLastError();
        if (err == EINTR) {
            return Write(buffer, bsize, flush);
        }

        if (err != WSAEWOULDBLOCK) {
            has_error = true;
        }
#else
        if(errno==EINTR)
        {
            return Write(buffer, bsize, flush);
        }

        if(errno!=EAGAIN && errno!=EWOULDBLOCK)
        {
            has_error=true;
        }
#endif
        return false;
    }
    return true;
}

bool Pipe::Write(const std::string &str, bool flush) {
    return Write(&str[0], str.size(), flush);
}

size_t Pipe::Read(std::string *ret) {
    char buffer[8192];
    size_t l = Read(buffer, 8192);
    if (l > 0) {
        ret->resize(l);
        memcpy((char *) ret->c_str(), buffer, l);
    } else {
        return 0;
    }
    return l;
}

bool Pipe::hasError() {
    return has_error;
}

SOCKET Pipe::getSocket() {
    return s;
}

bool Pipe::Write(JSON::Object obj, bool Compressed, bool flush) {
    return Write(obj.stringify(Compressed), flush);
}


