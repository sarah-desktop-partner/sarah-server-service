/*************************************************************************
*    Sarah - Desktop Partner
*    Copyright (C) 2019-2019 Ebrahim Karimi
*
*    This program is free software: you can redistribute it and/or modify
*    it under the terms of the GNU Affero General Public License as published by
*    the Free Software Foundation, either version 3 of the License, or
*    (at your option) any later version.
*
*    This program is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU Affero General Public License for more details.
*
*    You should have received a copy of the GNU Affero General Public License
*    along with this program.  If not, see <http://www.gnu.org/licenses/>.
**************************************************************************/


#ifndef SARAH_PIPE_H
#define SARAH_PIPE_H

#include "Interface/Pipe.h"


class Pipe : public IPipe {

public:
    explicit Pipe(SOCKET pSocket);


    size_t Read(char *buffer, size_t bsize) override;

    bool Write(const char *buffer, size_t bsize, bool flush) override;

    size_t Read(std::string *ret) override;

    bool Write(const std::string &str, bool flush) override;

    bool Write(JSON::Object obj,bool compressed, bool flush);

    bool hasError() override;

    SOCKET getSocket() override;


private:
    SOCKET s;
    bool has_error;

};


#endif //SARAH_PIPE_H