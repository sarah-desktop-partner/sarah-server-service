Sarah - Desktop Partner
=======================

Server Service
--------------
[![License](https://img.shields.io/badge/License-GPL%20v3%2B-blue.svg?style=flat-square)](https://gitlab.com/sarah-desktop-partner/sarah-server-service/blob/master/LICENSE)

Operate your smart phone remotely with [Sarah Desktop Partner]() which is cross-platform and open sourced, so you can change or redistribute it under terms of [GPL v3](https://gitlab.com/sarah-desktop-partner/sarah-server-service/blob/master/LICENSE)
