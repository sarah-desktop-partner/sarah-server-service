/*************************************************************************
*    Sarah - Desktop Partner
*    Copyright (C) 2019-2019 Ebrahim Karimi
*
*    This program is free software: you can redistribute it and/or modify
*    it under the terms of the GNU Affero General Public License as published by
*    the Free Software Foundation, either version 3 of the License, or
*    (at your option) any later version.
*
*    This program is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU Affero General Public License for more details.
*
*    You should have received a copy of the GNU Affero General Public License
*    along with this program.  If not, see <http://www.gnu.org/licenses/>.
**************************************************************************/


#include "SarahPacket.h"

SarahPacket::SarahPacket(std::string doc) {
    Json::Reader reader;
    reader.parse(doc,obj);
}

SarahPacket::SarahPacket(Json::Value val) {
    this->obj = val;
}

std::string SarahPacket::getType() {
    return obj.get("type","Unknown").asString();
}

bool SarahPacket::hasError() {
    return obj.isNull();
}

std::string SarahPacket::toString() {
    return obj.toStyledString();
}

Json::Value& SarahPacket::getJsonObj() {
    return obj;
}

