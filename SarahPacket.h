/*************************************************************************
*    Sarah - Desktop Partner
*    Copyright (C) 2019-2019 Ebrahim Karimi
*
*    This program is free software: you can redistribute it and/or modify
*    it under the terms of the GNU Affero General Public License as published by
*    the Free Software Foundation, either version 3 of the License, or
*    (at your option) any later version.
*
*    This program is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU Affero General Public License for more details.
*
*    You should have received a copy of the GNU Affero General Public License
*    along with this program.  If not, see <http://www.gnu.org/licenses/>.
**************************************************************************/


#ifndef SARAH_SARAHPACKET_H
#define SARAH_SARAHPACKET_H


#include "Interface/SarahPacket.h"
#include "json/json.h"
#include "json/jsonUrbackup.h"

class SarahPacket : public ISarahPacket {

public:
    SarahPacket(std::string);
    SarahPacket(Json::Value val);

    std::string getType() override;

    bool hasError() override;

    std::string toString() override;

    Json::Value& getJsonObj();

private:
    Json::Value obj;
};


#endif //SARAH_SARAHPACKET_H
