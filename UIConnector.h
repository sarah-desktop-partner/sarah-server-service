#pragma once

/*************************************************************************
*    Sarah - Desktop Partner
*    Copyright (C) 2019-2019 Ebrahim Karimi
*
*    This program is free software: you can redistribute it and/or modify
*    it under the terms of the GNU Affero General Public License as published by
*    the Free Software Foundation, either version 3 of the License, or
*    (at your option) any later version.
*
*    This program is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU Affero General Public License for more details.
*
*    You should have received a copy of the GNU Affero General Public License
*    along with this program.  If not, see <http://www.gnu.org/licenses/>.
**************************************************************************/


#ifndef SARAH_UICONNECTOR_H
#define SARAH_UICONNECTOR_H
#include "Abstract/Client.h"

class UIConnector
{
public:
	bool Write(const char* buffer, size_t bsize, bool flush) {
		if(client)
		return client->Write(buffer, bsize, flush);
		return false;
	};

	bool Write(const std::string& str, bool flush) {
		if (client)
		return client->Write(str, flush);
		return false;
	};

	bool Write(JSON::Object obj, bool Compressed = true, bool flush = true) {
		if (client)
		return client->Write(obj, Compressed, flush);
		return false;
	};

	AClient* getUIClient() {
		return client;
	}

	 void setUIClient(AClient* cli) {
		 this->client = cli;
	};

private:
	AClient* client;
};


#endif // !SARAH_UICONNECTOR_H
