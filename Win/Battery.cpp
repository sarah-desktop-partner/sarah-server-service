/*************************************************************************
*    Sarah - Desktop Partner
*    Copyright (C) 2019-2019 Ebrahim Karimi
*
*    This program is free software: you can redistribute it and/or modify
*    it under the terms of the GNU Affero General Public License as published by
*    the Free Software Foundation, either version 3 of the License, or
*    (at your option) any later version.
*
*    This program is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU Affero General Public License for more details.
*
*    You should have received a copy of the GNU Affero General Public License
*    along with this program.  If not, see <http://www.gnu.org/licenses/>.
**************************************************************************/


#include "../Interface/Battery.h"
#include "../Manager.h"
#include<WinSock2.h>
#include <Windows.h>
#include <iostream>

extern IManager *manager;
bool Battery::getBatteryStatus(BatteryStatus &bt) {

    SYSTEM_POWER_STATUS status;
    int rc = GetSystemPowerStatus(&status);
    if (rc != 0) {
        bt.isConnectedToAC = status.ACLineStatus == 1;
        switch (status.BatteryFlag) {
            case 1:
                bt.status = "High";
                break;
            case 2:
                bt.status = "Low";
                break;
            case 4:
                bt.status = "Critical";
                break;
            case 8:
                bt.status = "Charging";
                break;
            case 128:
                bt.status = "null";
                break;
            default:
                bt.status = "UNKNOWN";
        }
        bt.batteryPercentage = status.BatteryLifePercent;
        bt.batterySaving = status.SystemStatusFlag == 1;
    } else{
        manager->Log("Couldn't get power status with exit code : " + std::to_string(rc),LL_ERROR);
    }
    return rc!=0;
}

JSON::Object Battery::serializeToJson(const BatteryStatus& bt) {
    JSON::Object obj;
    obj.set("batterySaving",bt.batterySaving);
    obj.set("status",bt.status);
    obj.set("batteryPercentage",bt.batteryPercentage);
    obj.set("isConnectedToAC",bt.isConnectedToAC);
    return obj;
}
