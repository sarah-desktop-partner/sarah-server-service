/*************************************************************************
*    Sarah - Desktop Partner
*    Copyright (C) 2019-2019 Ebrahim Karimi
*
*    This program is free software: you can redistribute it and/or modify
*    it under the terms of the GNU Affero General Public License as published by
*    the Free Software Foundation, either version 3 of the License, or
*    (at your option) any later version.
*
*    This program is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU Affero General Public License for more details.
*
*    You should have received a copy of the GNU Affero General Public License
*    along with this program.  If not, see <http://www.gnu.org/licenses/>.
**************************************************************************/


#include <string>
#include "../Manager.h"

bool Manager::LoadDLL(const std::string& name) {
	HMODULE dll = LoadLibraryA(name.c_str());

	if (dll == NULL)
	{
		manager->Log("Loading DLL \"" + name + "\" failed. Error Code: " + std::to_string(GetLastError()), LL_ERROR);
		return false;
	}

	unload_handles.push_back(dll);

	LOADACTIONS load_func = NULL;
	load_func = (LOADACTIONS)GetProcAddress(dll, "LoadActions");
	unload_functions.insert(std::pair<std::string, UNLOADACTIONS>(name, (UNLOADACTIONS)GetProcAddress(dll, "UnloadActions")));

	if (load_func == NULL)
		return false;

	load_func(this);
	manager->Log("Loaded " + name + " Plugin");
	return true;
}
