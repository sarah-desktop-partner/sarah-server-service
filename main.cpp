/*************************************************************************
*    Sarah - Desktop Partner
*    Copyright (C) 2019-2019 Ebrahim Karimi
*
*    This program is free software: you can redistribute it and/or modify
*    it under the terms of the GNU Affero General Public License as published by
*    the Free Software Foundation, either version 3 of the License, or
*    (at your option) any later version.
*
*    This program is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU Affero General Public License for more details.
*
*    You should have received a copy of the GNU Affero General Public License
*    along with this program.  If not, see <http://www.gnu.org/licenses/>.
**************************************************************************/

#include <iostream>
#include "Manager.h"
#include "ConnectionAcceptor.h"
#include "BroadcastSender.h"
#include "settingsValues.h"
#include "FileLogger.h"

#include "stringtools.h"

#if defined _WIN32 && defined AS_SERVICE
#include "Win/win_service/nt_service.h"
using namespace nt;
#endif // _WIN32

#include "Client/UIClient.h"


IManager *manager = NULL;
ConnectionAcceptor<Client> *ca = NULL;
ConnectionAcceptor<UiClient> *caUI = NULL;

#if defined _WIN32 && defined AS_SERVICE
void WINAPI my_service_main(DWORD argc, char_* argv[])
{
    ILogger* lg = new FileLogger("D:\\Logs.txt", LL_DEBUG);
    manager->addLogger(lg);
    std::string hostname= "Sarah Project";
    if (!manager->getHostname(hostname)) {
        manager->Log("Couldn't get Hostname!", LL_WARN);
        hostname = "Sarah Project";
    }
    BroadcastSender b(hostname, BROADCAST_PORT);
    manager->createThread(&b, "Broadcast Sender");
    ca = new ConnectionAcceptor<Client>(TCP_CONNECTION_PORT,"Phones Gateway");
    caUI = new ConnectionAcceptor<UiClient>(UI_TCP_CONNECTION_PORT,"Modules");
    if (caUI!=NULL)
    manager->createThread(caUI, "UIClient");
    else manager->Log("Couldn't start Server for Modules");
    if (ca != NULL)
    {
        if (!ca->hasError())
        (*ca)();
    else manager->Log("Couldn't start Server for Phones Gateway");
    }
    else
    {
        Sleep(500);
    }
}

void my_stop_fcn(void)
{
    nt_service& service = nt_service::instance(L"Sarah");
    service.stop(0);
}
#endif // AS_SERVICE


void printLicense() {
    std::cout << SARAH_APPLICATION_FULL_NAME << " Copyright (C) 2019  Ebrahim Karimi" << std::endl <<
              "This program comes with ABSOLUTELY NO WARRANTY; for details type `warranty'." << std::endl <<
              "This is free software, and you are welcome to redistribute it" << std::endl <<
              "under certain conditions; see 'http://www.gnu.org/licenses/' for details"
              << std::endl;
}

void printWarranty() {
    std::cout
            << "THERE IS NO WARRANTY FOR THE PROGRAM, TO THE EXTENT PERMITTED BY APPLICABLE LAW. EXCEPT WHEN OTHERWISE STATED IN WRITING THE COPYRIGHT HOLDERS AND/OR OTHER PARTIES PROVIDE THE PROGRAM “AS IS” WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE ENTIRE RISK AS TO THE QUALITY AND PERFORMANCE OF THE PROGRAM IS WITH YOU. SHOULD THE PROGRAM PROVE DEFECTIVE, YOU ASSUME THE COST OF ALL NECESSARY SERVICING, REPAIR OR CORRECTION."
            << std::endl
            << "SEE 'http://www.gnu.org/licenses/' FOR DETAILS"
            << std::endl;
}

int main(int argc, char *argv[]) {
    manager = new Manager();

    char m[15];
    manager->getRandonmString(m, sizeof(m));
    ((Manager *) manager)->setUniqueID(std::string(m));
    std::string workingDir = ExtractFilePath(argv[0]);
    std::map<string, string> parameters;
    std::vector<std::string> plugins;
    std::string arg, argp;
    if (argc > 1)
        for (int i = 0; i < argc; i++) {
            arg = argv[i];
            if (argc > i + 1)
                argp = argv[i + 1];
            if (arg == "-l" || arg == "--log-file") {
                size_t pos = argp.find(',');
                if (pos > 0) {
                    std::string argLL = argp.substr(pos + (size_t) 1);
                    argp = argp.substr(0, pos);
                    LOG_LEVEL ll;
                    if (argLL == "DEBUG")
                        ll = LL_DEBUG;
                    else if (argLL == "WARN")
                        ll = LL_WARN;
                    else if (argLL == "ERROR")
                        ll = LL_ERROR;
                    else
                        ll = LL_INFO;
                    ILogger *loger = new FileLogger(argp, ll);
                    manager->addLogger(loger);
                }
            } else if (arg == "--working-dir") {
                workingDir = argp;
            } else if (arg == "--plugin" || arg == "-p") {
                plugins.push_back(argp);
            } else if (arg == "--license" || arg == "-c" || arg == "--copyright" ||
                       arg == "license" || arg == "copyright") {
                printLicense();
                return 0;
            } else if (arg == "warranty") {
                printWarranty();
                return 0;
            }
        }

    printLicense();
    if (!manager->setWorkingDirectory(workingDir)) {
        manager->Log("Cant Set Working Directory", LL_ERROR);
    }
    manager->setParameters(parameters);

    // Loading Plugins
    for (size_t i = 0; i < plugins.size(); ++i) {
        if (!manager->LoadDLL(plugins[i])) {
            manager->Log("Loading " + (std::string) plugins[i] + " failed", LL_ERROR);
        }
    }
#ifdef _WIN32

    // Setup for Sockets in Windows

    WSAData wsaData{};
    int rc = WSAStartup(MAKEWORD(2, 2), &wsaData);
    if (rc != 0)
    {
        manager->Log("Couldn't Initialize WSAStartup !", LL_ERROR);
        return rc;
    }
#endif
#ifndef AS_SERVICE

    std::string hostname;
    if (!manager->getHostname(hostname)) {
        manager->Log("Couldn't get Hostname!", LL_WARN);
        hostname = "Sarah Project";
    }
    BroadcastSender b(hostname, BROADCAST_PORT);
    manager->createThread(&b, "Broadcast Sender");
    caUI = new ConnectionAcceptor<UiClient>(UI_TCP_CONNECTION_PORT, "Modules");
    if (!caUI->hasError())
        manager->createThread(caUI, "UIClient");
    else manager->Log("Couldn't start Server for Modules");
    ca = new ConnectionAcceptor<Client>(TCP_CONNECTION_PORT, "Phones Gateway", false);
    if (!ca->hasError())
        (*ca)();
    else manager->Log("Couldn't start Server for Phones Gateway");
#else

    // creates an access point to the instance of the service framework
    nt_service& service = nt_service::instance(L"Sarah");

    // register "my_service_main" to be executed as the service main method
    service.register_service_main(my_service_main);

    // register "my_init_fcn" as initialization fcn
    //service.register_init_function(my_init_fcn);

    // config the service to accepts stop controls. Do nothing when it happens
    //service.accept_control( SERVICE_ACCEPT_STOP );
    service.register_control_handler(SERVICE_CONTROL_STOP, my_stop_fcn);

    // config the service to accepts shutdown controls and do something when receive it
    service.register_control_handler(SERVICE_CONTROL_SHUTDOWN, my_stop_fcn);

    service.start();

#endif // !AS_SERVICE

    return 0;
}