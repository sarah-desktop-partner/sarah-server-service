/*************************************************************************
*    Sarah - Desktop Partner
*    Copyright (C) 2019-2019 Ebrahim Karimi
*
*    This program is free software: you can redistribute it and/or modify
*    it under the terms of the GNU Affero General Public License as published by
*    the Free Software Foundation, either version 3 of the License, or
*    (at your option) any later version.
*
*    This program is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU Affero General Public License for more details.
*
*    You should have received a copy of the GNU Affero General Public License
*    along with this program.  If not, see <http://www.gnu.org/licenses/>.
**************************************************************************/


#ifndef SARAH_SETTINGSVALUES_H
#define SARAH_SETTINGSVALUES_H
#define SARAH_APPLICATION_FULL_NAME "Sarah - Desktop Partner"
#define SARAH_VERSION "1.0.0"
#define BROADCAST_PORT 23519
#define TCP_CONNECTION_PORT 52599
#define UI_TCP_CONNECTION_PORT 35192
#define AUTH_REPEAT_TIME 3

#define UI_PASSWD_FILE_NAME "ui_key"
#endif //SARAH_SETTINGSVALUES_H
