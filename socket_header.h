/*************************************************************************
*    Sarah - Desktop Partner
*    Copyright (C) 2019-2019 Ebrahim Karimi
*
*    This program is free software: you can redistribute it and/or modify
*    it under the terms of the GNU Affero General Public License as published by
*    the Free Software Foundation, either version 3 of the License, or
*    (at your option) any later version.
*
*    This program is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU Affero General Public License for more details.
*
*    You should have received a copy of the GNU Affero General Public License
*    along with this program.  If not, see <http://www.gnu.org/licenses/>.
**************************************************************************/


#ifndef SARAH_SOCKETHEADER_H
#define SARAH_SOCKETHEADER_H
#ifdef _WIN32
#	include <WS2tcpip.h>
#	include <Windows.h>
#	define MSG_NOSIGNAL 0
#	define socklen_t int
#else
#	include <signal.h>
#	include <sys/socket.h>
#	include <sys/types.h>
#	include <poll.h>
#	include <netinet/in.h>
#	include <netinet/tcp.h>
#	include <arpa/inet.h>
#	include <netdb.h>
#	include <unistd.h>
#	include <fcntl.h>
#	define SOCKET_ERROR -1
#	define closesocket close
#	define SOCKET int
#	define Sleep(x) usleep(x*1000)
#endif
#if defined(__sun__) || defined(__APPLE__)
#	define MSG_NOSIGNAL 0
#endif
#ifdef SOCK_CLOEXEC
#ifndef VERSION
//#include "config.h"
#endif
#ifdef HAVE_ACCEPT4
#define ACCEPT_CLOEXEC(sockfd, addr, addrlen) accept4(sockfd, addr, addrlen, SOCK_CLOEXEC)
#else
#define EMULATE_ACCEPT_CLOEXEC
#endif
#else
#ifdef _WIN32
#define ACCEPT_CLOEXEC(sockfd, addr, addrlen) accept(sockfd, addr, addrlen)
#else
#define EMULATE_ACCEPT_CLOEXEC
#endif //!_WIN32
#endif //!SOCK_CLOEXEC

#ifdef EMULATE_ACCEPT_CLOEXEC
#ifndef ACCEPT_CLOEXEC_DEFINED
#define ACCEPT_CLOEXEC_DEFINED
namespace {
	int ACCEPT_CLOEXEC(int sockfd, struct sockaddr *addr, socklen_t *addrlen) {
		int rc = accept(sockfd, addr, addrlen);
		if (rc) fcntl(rc, F_SETFD, fcntl(rc, F_GETFD, 0) | FD_CLOEXEC);
		return rc;
	}
}
#endif //ACCEPT_CLOEXEC_DEFINED
#endif //EMULATE_ACCEPT_CLOEXEC
#endif // SARAH_SOCKETHEADER_H